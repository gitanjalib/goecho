FROM golang:alpine3.10 as build
COPY . /go/src/app/
WORKDIR /go/src/app/
ENV GO111MODULE=on
RUN  go mod init && \
     go build main.go

FROM alpine:3.9.5
COPY --from=build /go/src/app/main /main

EXPOSE 8123

ENTRYPOINT  ./main
